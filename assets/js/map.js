var styles = [
{
	"featureType":"all",
	"elementType":"labels.text.fill",
	"stylers":[{"color":"#ffffff"}]
},
{
	"featureType":"all","elementType":"labels.text.stroke",
	"stylers":
	[
	{"color":"#000000"},
	{"lightness":13}
	]
},
{
	"featureType":"administrative",
	"elementType":"geometry.fill",
	"stylers":
	[{"color":"#000000"}]
},
{
	"featureType":"administrative",
	"elementType":"geometry.stroke",
	"stylers":[{"color":"#144b53"},
	{"lightness":14},{"weight":1.4}]
},
{
	"featureType":"landscape",
	"elementType":"geometry.fill",
	"stylers":
	[{"color":"#292E37"}]
},
{
	"featureType":"poi",
	"elementType":"geometry",
	"stylers":
	[{"color":"#0c4152"},
	{"lightness":5}]
},
{
	"featureType":"road.highway",
	"elementType":"geometry.fill",
	"stylers":[{"color":"#000000"}]
},
{
	"featureType":"road.highway",
	"elementType":"geometry.stroke",
	"stylers":[{"color":"#0b434f"},
	{"lightness":25}]
},{"featureType":"road.arterial","elementType":"geometry.fill","stylers":[{"color":"#000000"}]},{"featureType":"road.arterial","elementType":"geometry.stroke","stylers":[{"color":"#0b3d51"},{"lightness":16}]},{"featureType":"road.local","elementType":"geometry","stylers":[{"color":"#000000"}]},{"featureType":"transit","elementType":"geometry","stylers":[{"color":"#F26B68"}]},{"featureType":"water","elementType":"all","stylers":[{"color":"#021019"}]}]

var options = {
	mapTypeControlOptions: {
		mapTypeIds: [ 'Styled']
	},
	center: new google.maps.LatLng(19.421633, -99.162613),
	zoom: 13,
	mapTypeId: 'Styled',
	//zoomControl: false,
	streetViewControl: false,
	overviewMapControl: false,
	mapTypeControl: false,
};

var div = document.getElementById('map');
var map = new google.maps.Map(div, options);
var styledMapType = new google.maps.StyledMapType(styles, { name: 'Styled' });
var layer, layer2, layer3, layer4
var heatmap, heatmap2

function initialize(){
	map.mapTypes.set('Styled', styledMapType);

	var heatmap = new google.maps.visualization.HeatmapLayer({
		data: heatmapData
	});

	var gradient = [
	'rgba(0, 255, 255, 0)',
	'rgba(0, 255, 255, 1)',
	'rgba(0, 191, 255, 1)',
	'rgba(0, 127, 255, 1)',
	'rgba(0, 63, 255, 1)',
	'rgba(0, 0, 255, 1)',
	'rgba(0, 0, 223, 1)',
	'rgba(0, 0, 191, 1)',
	'rgba(0, 0, 159, 1)',
	'rgba(0, 0, 127, 1)',
	'rgba(63, 0, 91, 1)',
	'rgba(127, 0, 63, 1)',
	'rgba(191, 0, 31, 1)',
	'rgba(255, 0, 0, 1)'
	]

	var gradient2 = [
	'rgba(102,255,0,0)', 
	'rgba(147,255,0,1)', 
	'rgba(193,255,0,1)', 
	'rgba(238,255,0,1)', 
	'rgba(244,227,0,1)', 
	'rgba(244,227,0,1)', 
	'rgba(249,198,0,1)', 
	'rgba(255,170,0,1)', 
	'rgba(255,113,0,1)', 
	'rgba(255,57,0,1)', 
	'rgba(255,0,0,1)'
	]

	heatmap.set('radius', 15);
	heatmap.set('gradient', gradient2);
	//heatmap.setMap(map);
	//////////////////////////////////////////////////////////

	/*var heatmap2 = new google.maps.visualization.HeatmapLayer({
		data: heatmapData2
	});*/
	/*
	heatmap2.set('radius', 10);
	heatmap2.set('gradient', gradient2);
	heatmap2.set("opacity", 0.4);
	//heatmap2.setMap(map);
	*/

	/*layer = new google.maps.FusionTablesLayer({
		query: {
			select: "col7",
			from: "1_feIi3FzByJjmV18wgwDjwpvpz-4JoU0NxmMoWI",
		}
	});*/

heatmap.setMap(map);

	//layer.setMap(map);

	$('#incidentes').bind("change", function() {
		if($(this).is(":checked")) {
			heatmap.setMap(map);
		}
		else{
			heatmap.setMap(null);
		} 
	});

	$('.plog').click(function(){
		var latitud = $(this).data('lat')
		var longi = $(this).data('lon')
		var myLatlng = new google.maps.LatLng(latitud, longi);

		var marker = new google.maps.Marker({
			position: myLatlng,
			map: map
		});
		map.setCenter(myLatlng);
	})

	/*$('#baches').bind("change", function() {
		if($(this).is(":checked")) {
			heatmap2.setMap(map);
		}
		else{
			heatmap2.setMap(null);
		} 
	});*/
}

google.maps.event.addDomListener(window, 'load', initialize);