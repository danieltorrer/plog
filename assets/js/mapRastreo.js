
var styles = [
{
	"featureType":"all",
	"elementType":"labels.text.fill",
	"stylers":[{"color":"#ffffff"}]
},
{
	"featureType":"all","elementType":"labels.text.stroke",
	"stylers":
	[
	{"color":"#000000"},
	{"lightness":13}
	]
},
{
	"featureType":"administrative",
	"elementType":"geometry.fill",
	"stylers":
	[{"color":"#000000"}]
},
{
	"featureType":"administrative",
	"elementType":"geometry.stroke",
	"stylers":[{"color":"#144b53"},
	{"lightness":14},{"weight":1.4}]
},
{
	"featureType":"landscape",
	"elementType":"geometry.fill",
	"stylers":
	[{"color":"#292E37"}]
},
{
	"featureType":"poi",
	"elementType":"geometry",
	"stylers":
	[{"color":"#0c4152"},
	{"lightness":5}]
},
{
	"featureType":"road.highway",
	"elementType":"geometry.fill",
	"stylers":[{"color":"#000000"}]
},
{
	"featureType":"road.highway",
	"elementType":"geometry.stroke",
	"stylers":[{"color":"#0b434f"},
	{"lightness":25}]
},{"featureType":"road.arterial","elementType":"geometry.fill","stylers":[{"color":"#000000"}]},{"featureType":"road.arterial","elementType":"geometry.stroke","stylers":[{"color":"#0b3d51"},{"lightness":16}]},{"featureType":"road.local","elementType":"geometry","stylers":[{"color":"#000000"}]},{"featureType":"transit","elementType":"geometry","stylers":[{"color":"#F26B68"}]},{"featureType":"water","elementType":"all","stylers":[{"color":"#021019"}]}]

var options = {
	mapTypeControlOptions: {
		mapTypeIds: [ 'Styled']
	},
	center: new google.maps.LatLng(19.421633, -99.162613),
	zoom: 13,
	mapTypeId: 'Styled',
	//zoomControl: false,
	streetViewControl: false,
	overviewMapControl: false,
	mapTypeControl: false,
};

var div = document.getElementById('mapaRecorrido');
var map = new google.maps.Map(div, options);
var styledMapType = new google.maps.StyledMapType(styles, { name: 'Styled' });

function initialize(){
	map.mapTypes.set('Styled', styledMapType);

	$('.plog').click(function(){
		var latitud = $(this).data('lat')
		var longi = $(this).data('lon')
		var myLatlng = new google.maps.LatLng(latitud, longi);

		var marker = new google.maps.Marker({
			position: myLatlng,
			map: map
		});
		map.setCenter(myLatlng);
	})

	
	
}

google.maps.event.addDomListener(window, 'load', initialize);