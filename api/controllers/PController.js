/**
 * PController
 *
 * @description :: Server-side logic for managing ps
 * @help        :: See http://links.sailsjs.org/docs/controllers
 */

 module.exports = {

 	view: function(req, res){

 		Plog.find()
 		.where( req.param("id") )
 		.populate('ploggie')
 		.populate('puntos')
 		.exec(function (err, plog){

 			if (err) return res.serverError(err);
 			if (!plog) return res.serverError(err);
 			console.log(plog)
 			console.log('plog')
 			
 			/* Fix fox development */
 			req.user = [{
 				nombre: "Daniel"
 			}]

 			/*  */
 			
 			res.view('plog/find', {user: req.user[0], plog: plog})
 		});

		//res.view('plog/find', req.id);

	}

};

