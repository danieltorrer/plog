/**
 * UserController
 *
 * @description :: Server-side logic for managing users
 * @help        :: See http://links.sailsjs.org/docs/controllers
 */

 module.exports = {

 	index: function(req, res){

 		Plog.find({ sort: 'createdAt DESC' })
 		.populate('ploggie')
 		.exec(function findReports(err, plogs){
 			if (err) return res.serverError(err);
 			
 			/* Fix fox development */
 			req.user = [{
 				nombre: "Daniel"
 			}]
 			/*  */

 			console.log(plogs);

 			res.view('dashboard/graph', {user: req.user[0], plogs: plogs}); 			
 		})
 	}

 }
