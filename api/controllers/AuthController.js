var passport = require('passport');

module.exports = {

  login: function (req, res) {
    res.view();
  },

  logout: function (req, res){
    req.logOut()
    req.session.user = null;
    req.session.flash = 'You have logged out';
    res.redirect('/');
  },

  'facebook': function (req, res, next) {
     passport.authenticate('facebook', { scope: ['email', 'public_profile']},
        function (err, user) {
            req.logIn(user, function (err) {
            if(err) {
                console.log(err);
                req.session.flash = 'There was an error';
                res.redirect('/');
            } else {
                req.session.user = user;
                res.redirect('/dashboard');
            }
        });
    })(req, res, next);
  },

  'facebook/callback': function (req, res, next) {
     passport.authenticate('facebook',
        function (req, res) {
            res.redirect('/dashboard');
        })(req, res, next);
  }

};