/**
* Plog.js
*
* @description :: TODO: You might write a short summary of how this model works and what it represents here.
* @docs        :: http://sailsjs.org/#!documentation/models
*/

module.exports = {

	attributes: {
		emisor: {
			type : "string"
		},
		
		lat: {
			type : "string"
		},

		long: {
			type: "string"
		},

		/*contactoEmergencia:[{
			tipo:"" //WA, SMS, CALL,
			destinatarioNum:"num",
			destinatarioNom:"nombre"
		},*/
		/*{
			tipo:"" //WA, SMS, CALL,
			destinatarioNum:"num",
			destinatarioNom:"nombre"
		}],*/
		
		hora: {
			type: "string"
		},

		ploggie: {
			model: "usuario"
		},

		puntos: {
			collection: "punto",
			via: "miga"
		}
	}
};

