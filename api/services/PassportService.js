var passport = require('passport'),
FacebookStrategy = require('passport-facebook').Strategy;

exports.init = function(req){

  passport.serializeUser(function (user, done) {
    done(null, user.id);
  });

  passport.deserializeUser(function (id, done) {
    User.findById(id, function (err, user) {
      done(err, user);
    });
  });

  PassportService.initFacebook(req);
  //PassportService.initLocal(req);
}


exports.initFacebook = function(req){
  passport.use('facebook', new FacebookStrategy({
    clientID : sails.config.facebook.clientID,
    clientSecret : sails.config.facebook.clientSecret,
    callbackURL: sails.config.facebook.callbackURL,
    enableProof: false
  },

  function (accessToken, refreshToken, profile, done) {
    User.findOrCreate({facebookId : profile.id}, {facebookId : profile.id, email : profile._json.email, nombre :  profile._json.name }).exec(function createFindCB(err, user){
      if(err) {
        console.log(err)
        return done(err,null)
      }
        return done(null, user);
    })
  })
  );

}