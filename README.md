#PLOG
##Descripción

![a](http://i.imgur.com/8N6XmEa.png)

Plog es una herramienta de seguridad de fácil acceso a grupos poblacionales particularmente vulnerables como mujeres, niños y adultos mayores. Consta de dos partes, una aplicación móvil enfocada a la ciudadanía y una plataforma web para uso del gobierno. 

![Imgur](http://i.imgur.com/I1zrtnF.png)

La aplicación móvil, a través de su sistema de alertas permite al ciudadano tener un mayor acercamiento con los cuerpos de seguridad, lo cual mejora la capacidad de respuesta de los funcionarios a cargo de la seguridad y que repercute positivamente en los niveles de seguridad.

![](http://i.imgur.com/Qfl3lSd.png)


Además cuenta con un panel web en el que los datos generados por los usuarios se recolectan para posteriormente ser analizados y así poder focalizar mejor los recursos económicos y humanos en las zonas que lo requerían.

![a](http://imgur.com/iNJ1U4l.png)

 
 
##Dependencias

### :rocket: Showtime 

Debes tener instalado git, node.js, npm y tu gestor de base de datos favorito.

#### :one: Instalar Sails*

```bash
$ sudo npm install sails -g
```

#### :two: Clonar el proyecto

Clonamos este repositorio

```bash
$ git clone https://github.com/monkeythecoder/plog.git
```

Entramos al folder
```bash
$ cd plog
```
Instalamos todas las dependencias
```bash
$ npm install
```

#### :three: Configurar variables locales
Creamos un archivo en config/local.js

```bash
$ touch config/local.js
```

Y dentro de ese archivo establecemos las siguientes variables:
- Datos de conexión a la base de datos
- API Keys de facebook (Para el *Login with facebook*)

#### :four: Lanzar la aplicación
```bash
$ sails lift
```

Ya puedes ver tu aplicación en: [http://localhost:5000](http://localhost:5000)


## Producción
Para lanzar la aplicación en modo de producción, ejecuta el comando:
```bash
$ NODE_ENV=production node app.js
```

## API
El proyecto se encuentra en [heroku](https://plogapp.herokuapp.com) y cuenta con una API publica la cual permite crear usuarios y generar plogs (reportes)

#### PLOG
|Verb|Descripción|
|-----|----------|
|```GET /plog```|Listado de Plogs|
|```POST /plog```|Crear Plog|
|```PUT /plog/id```|Editar Plog |
|```DELETE /plog/id```|Eliminar Plog|

#### USUARIO
|Verb|Descripción|
|-----|----------|
|```GET /usuario```|Listado de Plogs|
|```POST /usuario```|Crear Plog|
|```PUT /usuario/id```|Editar Plog |
|```DELETE /usuario/id```|Eliminar Plog|
